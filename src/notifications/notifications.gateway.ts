import { Injectable } from '@nestjs/common';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { NotificationsService } from './notifications.service';

interface UserClientSocket {
  client: Socket;
  userId: number;
}

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
@Injectable()
export class NotificationsGateway
  implements OnGatewayConnection, OnGatewayDisconnect
{
  constructor(private notiSerivce: NotificationsService) {}

  @WebSocketServer()
  server: Server;

  private userSocketList: Array<UserClientSocket> = [];

  handleConnection(client: Socket) {
    console.log(`Client connected: ${client.id}`);
  }

  handleDisconnect(client: any) {
    console.log(`Client disconnected: ${client.id}`);
    // console.log(`--------------------`);
    this.userSocketList = this.userSocketList.filter(
      (c) => c.client.id !== client.id,
    );
  }

  @SubscribeMessage('associateUser')
  associateUserWithSocket(client: Socket, userId: number): void {
    console.log(`User id connected: ${userId}`);

    this.userSocketList.push({
      client: client,
      userId: userId,
    });
  }

  @SubscribeMessage('events')
  async handleNotification(payload: any) {
    const ids: Array<number> = payload.ids;
    const noti = payload.noti;

    for (const userId of ids) {
      const userNoti = await this.notiSerivce.createUserNotification(
        noti,
        userId,
      );
      const index = this.userSocketList.findIndex((c) => c.userId === userId);

      if (index !== -1) {
        this.userSocketList[index].client.emit('events', {
          noti: userNoti,
        });
      }
    }
  }

  @SubscribeMessage('post_comment_noti')
  async handleNotiComment(payload: any) {
    const ids: Array<number> = payload.ids;
    const noti = payload.noti;
    const comment = payload.comment;
    const type = payload.type;

    for (const userId of ids) {
      const userNoti = await this.notiSerivce.createUserNotification(
        noti,
        userId,
      );
      this.userSocketList.forEach((c) => {
        if (c.userId === userId) {
          c.client.emit('events', {
            noti: userNoti,
          });
          c.client.emit('post_comment_noti', {
            comment: comment,
            type: type,
          });
        }
      });
    }
  }
}
