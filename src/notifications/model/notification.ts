export interface NotificationModel {
  id: number;
  title: string;
  link: string;
  created_at: Date;
}
