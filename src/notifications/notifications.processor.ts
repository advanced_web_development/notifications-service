import {
  OnQueueActive,
  OnQueueCompleted,
  OnQueueRemoved,
  Process,
  Processor,
} from '@nestjs/bull';
import { Job } from 'bull';
import { NotificationsGateway } from './notifications.gateway';
import { NotificationsService } from './notifications.service';

@Processor('notifications')
export class NotificationsProcessor {
  constructor(
    private notisGateway: NotificationsGateway,
    private notisService: NotificationsService,
  ) {}

  @OnQueueActive()
  onActice(job: Job) {
    console.log(`Processing job ${job.id} of type: ${job.name}`);
  }

  @OnQueueCompleted()
  onCompleted(job: Job) {
    job.remove();
  }

  @OnQueueRemoved()
  onRemove(job: Job) {
    console.log(`remove job id:${job.id}`);
  }

  @Process('create_class_post')
  async handleCreateClassPostNotification(job: Job) {
    const payload = job.data;
    const classPost = payload.classPost;
    const title = payload.title;
    const link = payload.link;
    const type = payload.type;

    if (type === 'exam-review') {
      const userIds = payload.receiverIds;
      const noti = await this.notisService.createClassNotification(
        classPost.class_id,
        classPost.authorId,
        title,
        link,
      );
      this.notisGateway.handleNotification({
        ids: userIds,
        noti: noti,
        post: classPost,
        type: type,
      });
      return 'ok';
    } else {
      const userIds = await this.notisService.getUserIdsInClassToSendNoti(
        classPost.class_id,
        classPost.author_id,
      );
      const noti = await this.notisService.createClassNotification(
        classPost.class_id,
        classPost.author_id,
        title,
        link,
      );

      this.notisGateway.handleNotification({
        ids: userIds,
        noti: noti,
        post: classPost,
        type: type,
      });
      return 'ok';
    }
  }

  @Process('create_class_post_comment')
  async handleCreatePostCommentNotification(job: Job) {
    const payload = job.data;
    const receiverIds = payload.receiverIds;
    const title = payload.title;
    const link = payload.link;
    const comment = payload.comment;
    const type = payload.type;

    const noti = await this.notisService.createCommentNoti(link, title);

    this.notisGateway.handleNotiComment({
      ids: receiverIds,
      noti: noti,
      comment: comment,
      type: type,
    });
    return 'ok';
  }
}
