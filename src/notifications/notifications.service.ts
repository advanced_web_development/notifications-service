import { Injectable } from '@nestjs/common';
import { appConfig } from 'src/config';
import { PrismaService } from 'src/prisma/prisma.service';
import { NotificationModel } from './model/notification';

@Injectable()
export class NotificationsService {
  constructor(private prismaService: PrismaService) {}

  async createUserNotification(noti: NotificationModel, userId: number) {
    const oldUserNoti = await this.prismaService.userNotification.findUnique({
      where: {
        user_id_notification_id: {
          user_id: userId,
          notification_id: noti.id,
        },
      },
    });
    if (oldUserNoti) {
      await this.prismaService.userNotification.update({
        where: {
          user_id_notification_id: {
            user_id: userId,
            notification_id: noti.id,
          },
        },
        data: {
          is_seen: false,
        },
      });

      return await this.prismaService.userNotification.findUnique({
        where: {
          user_id_notification_id: {
            user_id: userId,
            notification_id: noti.id,
          },
        },
        include: {
          Notification: true,
        },
      });
    }

    const userNoti = await this.prismaService.userNotification.create({
      data: {
        user_id: userId,
        notification_id: noti.id,
      },
    });

    return await this.prismaService.userNotification.findUnique({
      where: {
        user_id_notification_id: {
          user_id: userId,
          notification_id: userNoti.notification_id,
        },
      },
      include: {
        Notification: true,
      },
    });
  }

  async createClassNotification(
    classId: string,
    authorId: number,
    title: string,
    link: string,
  ) {
    const classById = await this.prismaService.class.findUnique({
      where: {
        id: classId,
      },
    });

    const author = await this.prismaService.user.findUnique({
      where: {
        id: authorId,
      },
    });

    if (classById === null || author === null) {
      return null;
    }

    const noti = await this.prismaService.notification.create({
      data: {
        title: title,
        link: link,
      },
    });

    return noti;
  }

  async getUserIdsInClassToSendNoti(classId: string, authorId: number) {
    //only user join in class can get notifications
    const usersInClass = await this.prismaService.enrollment.findMany({
      select: {
        user_id: true,
      },
      where: {
        class_id: classId,
        user_id: {
          not: authorId,
        },
      },
    });
    const userIds = usersInClass.map((s) => s.user_id);

    return userIds;
  }

  async getDetailUserNoti(userId: number, notiId: number) {
    const detail = await this.prismaService.userNotification.findUnique({
      where: {
        user_id_notification_id: {
          user_id: userId,
          notification_id: notiId,
        },
      },
      include: {
        Notification: true,
      },
    });
    return detail;
  }

  async createCommentNoti(link: string, title: string) {
    const noti = await this.prismaService.notification.findFirst({
      where: {
        title: title,
        link: link,
      },
    });
    if (noti) {
      return await this.prismaService.notification.update({
        where: {
          id: noti.id,
        },
        data: {
          created_at: new Date(),
        },
      });
    }
    return await this.prismaService.notification.create({
      data: {
        title: title,
        link: link,
      },
    });
  }
}
