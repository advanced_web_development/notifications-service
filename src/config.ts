require('dotenv').config();
class AppConfig {
  DATABASE_URL: string = process.env.DATABASE_URL;
  NOTIFICATION_QUEUE_URL = process.env.NOTIFICATION_QUEUE_URL;
  ROLE_CREATOR: string = process.env.ROLE_CREATOR;
  ROLE_TEACHER: string = process.env.ROLE_TEACHER;
  ROLE_STUDENT: string = process.env.ROLE_STUDENT;
}

export const appConfig = new AppConfig();
