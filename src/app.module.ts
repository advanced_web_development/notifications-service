import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NotificationsModule } from './notifications/notifications.module';
import { BullModule } from '@nestjs/bull';
import { appConfig } from './config';

@Module({
  imports: [
    NotificationsModule,
    BullModule.forRoot({
      redis: appConfig.NOTIFICATION_QUEUE_URL,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
